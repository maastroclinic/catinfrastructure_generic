CAT Infrastructure General
=========================================================

This repository is for the generic implementation configuration of the CAT system.
For site-specific configurations, please fork this repository, and work in a local branch of the forked repository