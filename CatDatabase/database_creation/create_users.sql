-- Create Kettle Spoon user with select rights
CREATE USER spoon_cat_db_select WITH PASSWORD 'changeme';
GRANT CONNECT ON DATABASE cat_db TO spoon_cat_db_select;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO spoon_cat_db_select;

-- Create Kettle Spoon user with edit rights
CREATE USER spoon_cat_db_update WITH PASSWORD 'changeme';
GRANT CONNECT ON DATABASE cat_db TO spoon_cat_db_update;
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE, TRIGGER ON ALL TABLES IN SCHEMA public TO spoon_cat_db_update;
GRANT SELECT, UPDATE ON ALL SEQUENCES IN SCHEMA public TO spoon_cat_db_update;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA public TO spoon_cat_db_update;

ALTER FUNCTION fn_getObjectForValue(character varying, character varying, character varying)
  OWNER TO cat_admin;